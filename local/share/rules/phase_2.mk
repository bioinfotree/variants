### phase_2.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

RAW_READS ?=

MISA_DEFINITION ?=
MISA_INTERRUPTIONS ?=



# orf prediction
extern ../../../../annotation/dataset/$(PRJ)/phase_5/orf_predictor.fasta as ORF_PRED_FASTA
# orf from blast results
extern ../../../../annotation/dataset/$(PRJ)/phase_1/nr.tab as NR_BLAST_TAB
# use only contigs, avoids singletons
extern ../../../../454_assembly/dataset/$(PRJ)/phase_2/$(PRJ_PREFIX)_1_as_unpad_nosing.fasta as 1_NO_SINGLETONS_FASTA


ifdef RAW_READS
# trim raw reads
sequences.fasta.results sequences.fasta.log:
	est_trimmer $(RAW_READS) -amb=5,10 -tr5=A,5,10 -tr3=A,5,10 -tr5=T,5,10 -tr3=T,5,10; \
	touch $@
else
sequences.fasta.results:
	ln -sf $(1_NO_SINGLETONS_FASTA) $@
endif

orf_predictor.fasta: $(ORF_PRED_FASTA)
	ln -sf $< $@

nr.tab: $(NR_BLAST_TAB)
	ln -sf $< $@

# prepare misa config file

# 	In a single line beginning with "def" a sequence of number pairs is expected,
#       whereas the first number defines the unit sizes and the second number the 
#       lower threshold of repeats for that specific unit.

# 	In a single line beginning with "int" a single number is expected defining 
#       the maximal number of bases between two adjacent microsatellites to be 
#       recognised as being a compound microsatellite.
misa.ini:
	> $@; \
	printf "definition(unit_size,min_repeats):\t$(MISA_DEFINITION)\n" >> $@; \
	printf "interruptions(max_difference_between_2_SSRs):\t$(MISA_INTERRUPTIONS)\n" >> $@; \



# run misa

# Results of the microsatellite search are stored in two files:

#     1) "<FASTfile>.misa" the localization and type of identified microsatellite(s) are stored in a tablewise manner.
#     2) "<FASTfile>.statistics" summarizes different statistics as the frequency of a specific microsatellite type according to the unit size or individual motifs.
#        Frequency of identified SSR motifs: repetition frequency
#        Frequency of classified repeat types (considering sequence complementary):
#        repetition frequency considering repetition, its reverse complementary, in one
#        in one strand and the other: AC/GT + CA/TG
%.results.misa %.results.statistics: %.results misa.ini
	misa $< $^2; \
	touch $@



# covert misa output into Primer3 Boulder-io compatible format (input/output format used as a program-to-program data interchange format in many information systems at the Whitehead/MIT Center for Genome Research) in order to design primers flanking the microsatellite locus.
# the original <FASTAfile> needs to be accessible as well.
%.results.p3in: %.results.misa %.results
	p3_in $<


# run primer3_core
%.results.p3out: %.results.p3in
	primer3 < $< > $@



# This tool parses the Primer3 output file (FASTAfile.p3out) for the calculated primer sequences, their position and melting temperatures as well as the expected PCR product size and merges this information together with the data from the file <FASTAfile.misa> creating a new file (FASTAfile.results).
%.results.p3misa: %.results.p3out %.results.misa
	p3_out $< $^2; \
	mv $*.results.results $@


# select columns from blast result of contings against NCBI nr
# selected fileds are: seq_ID, query_end, query_start
nr_hit_dict.tab: nr.tab
	cut -f 6,21,22 < $< \
	| bawk '!/^[$$,\#+]/ {  \
	if ($$2 < $$3) \
	{ \
	printf "%s\t%i\t%i\n", $$1, $$2, $$3; \
	} \
	else \
	{ printf "%s\t%i\t%i\n", $$1, $$3, $$2; } \
	}' > $@

# from fasta file of predicted orf from sequences that do not have a match
# against NCBI nr, I estract seq_ID, ORF_start, ORF_end, strand
orf_dict.tab: orf_predictor.fasta
	fasta2tab <$< \
	| bawk '!/^[$$,\#+]/ {  \
	if ($$3 < $$4) \
	{ \
	printf "%s\t%i\t%i\n", $$1, $$3, $$4; \
	} \
	else \
	{ printf "%s\t%i\t%i\n", $$1, $$4, $$3; } \
	}' > $@





# List of putative ORF coordinate for SSR-containing contigs

# For each contig containing SSRs the coordinates of putative orf contained in it are added. The ORFS can come from BLAST or prediction. If there is no ORF coordinates will be 0 0. Check that the coordinates are compatible with the length of the contig.
%.results.orf.tab: %.results.misa nr_hit_dict.tab orf_dict.tab %.results
	sed '1d' $< \
	| cut -f1 \
	| bsort \
	| uniq \
	| translate -a -v $^2 1 \   * add corresponding coordinate from blast *
	| translate -a -v $^3 1 \   * add corresponding coordinate from prediction *
	| translate -a -v <(fastalen $^4) 1 \   * add lenghts *
	| sed -e 's/\t\+/\t/g' \   * sobstitute multiple tabs with one tab *
	| bawk 'function abs(x) {return ((x < 0.0) ? -x : x)} !/^[$$,\#+]/ \
	{ if ( abs($$4-$$3) > $$2 ) { exit 1; } \   * check that the coordinates indicate actually a region internal to the sequence *
	if ( ($$3 ~/[0-9]/) && ($$4 ~/[0-9]/) ) { print $$0 } \   * Some sequences may have no orf. Kill them *
	}' \
	| sed 1'i\#SeqI\tSeqLength\tmin_coordinate\tmax_coordinate' > $@   * add column names *


# List if overlap fractions for SSR in SSR-containing contigs with ORFs

# merges the coordinates of the ORFs to those of the SSRs and calculates the fraction of overlap between ORF regions and SSR regions.
%.results.overlaps.tab: %.results.misa %.results.orf.tab
	sed '1d' $< \
	| bsort --key=1,1 \
	| translate -a -k <(sed '1d' $^2 | bsort --key=1,1) 1 \
	| select_columns 3 4 9 10 1 5 \   * min_orf_coord max_orf_coord min_SSR_coord max_SSR_coord contig SSR_num *
	| region_overlap --already-sorted >$@


%.results.overlaps.info: %.results.overlaps.tab
	bawk 'BEGIN {complete=0; partially=0; total=0; \
	print "SSR_completely_in_orfs","SSR_partially_in_orfs","total_SSR_in_SSR-containing_contigs_with_ORF"; \
	} !/^[$$,\#+]/ {  \
	if ($$3 == 1 ) { complete++; } \
	if ($$3 < 1 && $$3 > 0) { partially++; } \
	total++; } \
	END { print complete, partially, covered, total; }' <$< >$@



.PHONY: test
test:
	@echo $(454_INPUT)


# Standard Phony Targets for Users.

# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += sequences.fasta.results \
	 sequences.fasta.results.misa \
	 sequences.fasta.results.statistics \
	 sequences.fasta.results.p3misa \
	 sequences.fasta.results.orf.tab \
	 sequences.fasta.results.overlaps.tab \
	 sequences.fasta.results.overlaps.info



# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += nr_hit_dict.tab \
		orf_dict.tab \
		sequences.fasta.results.p3in \
		sequences.fasta.results.p3out \




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += sequences.fasta \
	 misa.ini \
	 sequences.fasta.log \
	 sequences.fasta.results.orf.tab \
	 sequences.fasta.results.overlaps.tab \
	 sequences.fasta.results.overlaps.info \
	 nr.tab \
	 orf_predictor.fasta



######################################################################
### phase_2.mk ends here
