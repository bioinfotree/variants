### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:


#context


TEST ?=
CPUS ?=
PRJ ?=
SAM_PATH ?=

# use only contigs, avoids singletons
extern ../../../../454_assembly/dataset/$(PRJ)/phase_2/$(PRJ_PREFIX)_1_as_unpad_nosing.fasta as 1_NO_SINGLETONS_FASTA

# orf prediction
extern ../../../../annotation/dataset/$(PRJ)/phase_5/orf_predictor.fasta as ORF_PRED_FASTA

# orf from blast results
extern ../../../../annotation/dataset/$(PRJ)/phase_1/nr.tab as NR_BLAST_TAB

# Ka/Ks > 1 genes evaluation
extern ../../../../annotation/dataset/$(PRJ)/phase_2/danio_EST.gene.lst as GENE_DANIO_HIT_LST
extern ../../../../annotation/dataset/$(PRJ)/phase_2/danio_EST.subject.lst as CDNA_DANIO_HIT_LST
extern ../../../../annotation/dataset/$(PRJ)/phase_2/danio_EST.tab as DANIO_HIT_TAB
extern ../../../../annotation/dataset/$(PRJ)/phase_6/b2go.GAF as B2GO_TAB



# links
reference.fasta: $(1_NO_SINGLETONS_FASTA)
	ln -sf $< $@


reference.lst: reference.fasta
	fasta2tab < $< | cut -f 1 > $@


orf_predictor.fasta: $(ORF_PRED_FASTA)
	ln -sf $< $@

nr.tab: $(NR_BLAST_TAB)
	ln -sf $< $@


# create log dir
log:
	mkdir -p $@;


# flow control does not seem to work when the script is inside make. 
# Since the script is complicated, it would be useful to run as an 
# independent script. 
# In this way would be more easily readable and flow control work.
VAR = var
$(VAR).flag: reference.fasta reference.lst log
	freebayes_pipe -s $(SAM_PATH) -r $< -g $^2 -l $^3 -v $(basename $@); \
	touch $@


# list sequences that has not a vcf file
unprocessed.lst: reference.lst $(VAR).flag
	comm -1 -3 \
	<(ls -1 var \
	| bawk '!/^[$$,\#+]/ { \
	split($$0,a,"."); \
	print a[1]; \
	}' | bsort) \
	<(bsort $<) > $@



# join all vcf files
var.vcf: $(VAR).flag
	CHUNCKS=`ls -1 $(basename $<)/*.vcf \
	| bawk '!/^[$$,\#+]/ { \
	{ printf "%s " , $$0; } \
	}'`; \
	vcf-concat $$CHUNCKS > var.vcf



# divides SNPs from INDELS
SNP.vcf: var.vcf
	INDEL_FILE="INDEL$(suffix $@)"; \
	bawk  -v SNP_file=$@ -v INDEL_file=$$INDEL_FILE '/^#/    { \
	print $$0 > SNP_file; \
	print $$0 > INDEL_file; \
	next; \
	} \
	/^[^\t]+\t[0-9]+\t[^\t]*\t[atgcATGC]\t[a-zA-Z]\t/   { \
	print $$0 > SNP_file; \
	next; \
	} \
	{ \
	print $$0 > INDEL_file; \
	next; \
	}' $<; \
	touch $$INDEL_FILE


# fake target
INDEL.vcf: SNP.vcf
	touch $@




# # freebayes also produces vcf file for SAMs whit no variants
# # they only contains comments
# # I remove them
# %_rm_empty.flag: %.flag
# 	cd $(basename $<); \
# 	for FILE in *.vcf ; do \
# 	bawk -v vcf_file=$$FILE 'BEGIN { count=0; } \
# 	!/^[$$,\#+]/ {  \
# 	if(NF!=0) \
# 	{count++;} \
# 	} END{ if (count==0) { system("rm " vcf_file); } }' $$FILE; \
# 	done; \
# 	cd ..; \
# 	touch $@;




# # this will intended to be a filter in order to discover SNPs or INDELS
# # near homopolymer repeats, i.e. the same base repeated N times.
# # I wanted to use vcftools with --get-INFO REPEAT option that generate a
# # tab separated file called *.vcf.INFO with CHROM	POS	REF	ALT	REPEAT
# # Repeats is in the form CA:2|CAGCA:2 and i intended to test if CA was made by a
# # single letter and if 2 is > 4.
# # I desisted the intent because i don't know how remove selected SNP
# %_filter.flag: %.flag
# 	cd $(basename $<); \
# 	for FILE in *.vcf ; do \
# 	vcftools --get-INFO REPEAT --vcf $$FILE --out $$FILE; \
# 	SEQ=$${FILE%.*}; \
# 	bawk -v id=$$SEQ -v file=$$FILE '!/^[$$,\#+]/ {  \
# 	if(NR>1) \
# 	{ \
# 	print "##"; \
# 	split($$5,b,"|"); \
# 	for (i in b) \
# 	{ \
# 	split(b[i],a,":"); \
# 	if ( ( a[1] ~ /[atgcATGC]/ ) &&  ( a[2] > 4 ) ) \
# 	{ print $$1, $$2 > file".remove"; }; \
# 	} \
# 	} \
# 	}' $$FILE.INFO; \
# 	vcftools --exclude-positions $$FILE.remove --vcf $$FILE --out $$FILE; \
# 	done; \
# 	cd ..; \
# #	touch $@;






%.table: %.vcf reference.fasta log
	GenomeAnalysisTK -S LENIENT --unsafe --log_to_file $^3/VariantsToTable.log -R $^2 -T VariantsToTable --allowMissingData -V $< \
	-F CHROM -F POS -F REF -F ALT -F REPEAT \
	-o $@


NUM_REPEAT = 4
%.removable: %.table
	_comment () { echo -n ""; }; \
	_comment "change only on the first occurrence"; \
	sed --in-place=".bk" '0,/CHROM/s//#CHROM/' $<; \
	bawk '!/^[$$,\#+]/ { \
	split($$5,b,"|"); \
	for (i in b) \
	{ \
	split(b[i],a,":"); \
	if ( ( a[1] ~ /[atgcATGC]/ ) &&  ( a[2] >= $(NUM_REPEAT) ) ) \
	{ print $$1, $$2; }; \
	} \
	}' $< > $@



%.filtered.vcf: %.vcf %.removable
	grep -f $^2 --fixed-strings --word-regexp --invert-match $< > $@


# # vcftools are implemented for diploid organisms
# # when vcf-validator is used on vcs by freebayes with --ploidy 8 errors are found:
# # CDNA3-4_11_2010_0_c10029:102 .. AN is 8, should be 2
# # CDNA3-4_11_2010_0_c10029:102 .. AC is 4, should be 0
# # ...
# # here I sobstitute AN and AC values
# # this filter now is unusable
# %_correct.flag: %.flag %_rm_empty.flag
# 	mkdir -p $(basename $@); \
# 	cd $(basename $<); \
# 	for FILE in *.vcf ; do \
# 	sed 's/;AN=8;/;AN=2;/g' $$FILE \
# 	| sed 's/;AC=4:/;AC=0;/g' > ../$(basename $@)/$$FILE; \
# 	vcftools --freq --counts --vcf $$FILE --out ../$(basename $@)/$$FILE; \
# 	done; \
# 	cd ..; \
# 	touch $@



# command vcftools --TsTv-by-count --vcf ../$(basename $@)/$$FILE for
# Calculation the Transition/Transversion ratio as a function of alternative allele count. Only uses bi-allelic SNPs. The resulting output file has the suffix '.TsTv.count'.
# doesn't work with polyploids
# Error:Polypolidy found, and not supported: CDNA3-4_11_2010_0_c10592:209


# calculate (Ts+1)/(Tv+1) allow the ratio to be calculated for contigs that had a zero for either TS or TV values
# join together summary and depth for the same sequence
# %_summary.lst: %.flag %_rm_empty.flag log
# 	_comment () { echo -n ""; }; \
# 	echo -e "#ID\tFILTER\tN_SNP/INDELs\tN_Ts\tN_Tv\tTs/Tv\t(Ts+1)/(Tv+1)\tINDV\tN_SITES\tMEAN_COV" > $@; \
# 	mkdir -p $(basename $@); \
# 	cd $(basename $<); \
# 	for FILE in *.vcf ; do \
# 	SAM=$$(basename $$FILE); \
# 	SEQ=$${SAM%.*}; \
# 	vcftools --depth --FILTER-summary --vcf $$FILE --out ../$(basename $@)/$$FILE; \
# 	_comment "join together summary and depth for the same sequence"; \
# 	paste \
# 	<(bawk -v id=$$SEQ '!/^[$$,\#+]/ {  \
# 	if(NR==2) \
# 	{print id, $$0, ($$3+1)/($$4+1);} \
# 	}' ../$(basename $@)/$$FILE.FILTER.summary) \
# 	<(bawk -v id=$$SEQ '!/^[$$,\#+]/ {  \
# 	if(NR==2) \
# 	{print $$0;} \
# 	}' ../$(basename $@)/$$FILE.idepth) >> ../$@; \
# 	mv ../$(basename $@)/$$FILE.log ../$^3/$$FILE.FILTER.summary.log; \
# 	done; \
# 	cd ..;



# does not function
%_summary.lst: %.filtered.vcf log
	_comment () { echo -n ""; }; \
	CHR=`bawk '!/^[$$,\#+]/ { print $$1; }' $< | bsort --key=1,1 | uniq`; \
	echo -e "#ID\tFILTER\tN_SNP/INDELs\tN_Ts\tN_Tv\tTs/Tv\t(Ts+1)/(Tv+1)\tINDV\tN_SITES\tMEAN_COV" > $@; \
	mkdir -p $(basename $@); \
	for SEQ in $$CHR ; do \
	vcftools --chr $$SEQ --depth --FILTER-summary --vcf $< --out $(basename $@)/$$SEQ; \
	_comment "join together summary and depth for the same sequence"; \
	paste \
	<(bawk -v id=$$SEQ '!/^[$$,\#+]/ {  \
	if(NR==2) \
	{print id, $$0, ($$3+1)/($$4+1);} \
	}' $(basename $@)/$$SEQ.FILTER.summary) \
	<(bawk -v id=$$SEQ '!/^[$$,\#+]/ {  \
	if(NR==2) \
	{print $$0;} \
	}' $(basename $@)/$$SEQ.idepth) >> $@; \
	mv $(basename $@)/$$SEQ.log $^2/$$SEQ.FILTER.summary.log; \
	done;




%_quality.lst: %.filtered.vcf log
	_comment () { echo -n ""; }; \
	CHR=`bawk '!/^[$$,\#+]/ { print $$1; }' $< | bsort --key=1,1 | uniq`; \
	echo -e "#ID\tPOS\tPHRED_QUAL\tP_WRONG\tMEAN_DEPTH\tVAR_DEPTH" > $@; \
	mkdir -p $(basename $@); \
	for SEQ in $$CHR ; do \
	vcftools --chr $$SEQ --site-mean-depth --site-quality --vcf $< --out $(basename $@)/$$SEQ; \
	_comment "p = 10^(-Q/10) is the inverse of phread score"; \
	paste \
	<(bawk -v id=$$SEQ '!/^[$$,\#+]/ {  \
	if(NR>1) \
	{printf "%s\t%4.3e\n", $$0, 10^(-$$3/10);} \
	}' $(basename $@)/$$SEQ.lqual) \
	<(bawk -v id=$$SEQ '!/^[$$,\#+]/ {  \
	if(NR>1) \
	{printf "%s\t%s\n", $$3, $$4;} \
	}' $(basename $@)/$$SEQ.ldepth.mean) >> $@; \
	mv $(basename $@)/$$SEQ.log $^2/$$SEQ.lqual.log; \
	done; \



%.info: %_summary.lst reference.fasta
	_comment () { echo -n ""; }; \
	REF_LEN=`fastalen < $^2 | stat_base --total 2`; \
	_comment "delete the first line which is header"; \
	paste -s -d "\n" \
	<(sed '1d' $< | stat_base --mean --median --stdev --min --max --total 3 \
	| bawk -v ref_len=$$REF_LEN '!/^[$$,\#+]/ {  \
	print "mean_VAR/contg\tmedian_VAR/contg\tstdev\tmin\tmax\ttotal_VAR\ttotal_ref_len\tmean_VAR/bp"; \
	if ($$6 == 0) {$$6=1;} \
	printf "%s\t%i\t%.4f\n", $$0, ref_len, ref_len/$$6; \
	}') \
	<(sed '1d' $< | stat_base --mean --median --stdev --min --max --total 4 \
	| bawk -v ref_len=$$REF_LEN '!/^[$$,\#+]/ {  \
	print "mean_Ts/contg\tmedian_Ts/contg\tstdev\tmin\tmax\ttotal_Ts\ttotal_ref_len\tmean_Ts/bp"; \
	if ($$6 == 0) {$$6=1;} \
	printf "%s\t%i\t%.4f\n", $$0, ref_len, ref_len/$$6; \
	}') \
	<(sed '1d' $< | stat_base --mean --median --stdev --min --max --total 5 \
	| bawk -v ref_len=$$REF_LEN '!/^[$$,\#+]/ {  \
	print "mean_Tv/contg\tmedian_Tv/contg\tstdev\tmin\tmax\ttotal_Tv\ttotal_ref_len\tmean_Tv/bp"; \
	if ($$6 == 0) {$$6=1;} \
	printf "%s\t%i\t%.4f\n", $$0, ref_len, ref_len/$$6; \
	}') \
	<(sed '1d' $< \
	| bawk 'BEGIN { count_over_1=0; count_under_1=0; print "(Ts+1)/(Tv+1)>=1", "(Ts+1)/(Tv+1)<1";} !/^[$$,\#+]/ { \
	if ($$7 >= 1) {count_over_1++;} \
	if ($$7 < 1) {count_under_1++;} \
	} END {print count_over_1++, count_under_1++}') > $@




# print ditribution of SNP and INDEL across contigs
# remove first line
SNP-INDEL_distrib.pdf: SNP_summary.lst INDEL_summary.lst
	paste <(sed '1d' $< | cut -f 3) <(sed '1d' $^2 | cut -f 3) \
	| this_distrib_plot --type histogram --remove-na --title "SNPs and INDELs distributions across contigs" --xlab "bins" --ylab "contigs (#)" --legend-label="SNPs","INDELs" --output=$@ 1 2


Ts-Tv_distrib.pdf: SNP_summary.lst
	bawk '!/^[$$,\#+]/ { \
	print log($$7)/log(10); \
	}' $< | Ts-Tv_distrib_plot --type histogram --title "log10 distribution of Ts/Tv for contigs containing SNP" --breaks=20 --xlab "log10(Ts/Tv)" --ylab "contigs (#)" --legend-label="SNPs" --color=2 --output=$@ 1




%_qual.pdf: %_quality.lst
	cut -f 3 $< | distrib_plot --type histogram \
	--output=$@ 1






# from filtered vcf file, i select columns
SNP_var.tab: SNP.filtered.vcf
	bawk 'BEGIN { printf "#SeqID\ttype\tstart\tend\talleles\n"; } \
	!/^[$$,\#+]/ {  \
	printf "%s\tSNP\t%i\t%i\t%s/%s\n", $$1, $$2, $$2, $$4, $$5; \
	}' $< > $@


# select columns from blast result of contings against NCBI nr
# selected fileds are: seq_ID, query_end, query_start
nr_hit_dict.tab: nr.tab
	cut -f 6,21,22 < $< \
	| bawk '!/^[$$,\#+]/ {  \
	printf "%s\t[%i-%i]\n", $$1, $$2, $$3; \
	}' > $@

# from fasta file of predicted orf from sequences that do not have a match
# against NCBI nr, I estract seq_ID, ORF_start, ORF_end, strand
orf_dict.tab: orf_predictor.fasta
	fasta2tab <$< \
	| bawk '!/^[$$,\#+]/ {  \
	printf "%s\t[%i-%i,%s]\n", $$1, $$3, $$4, $$2; \
	}' > $@

# to the list of unique contig whit SNP adds:
# length of contig, blastHit interval, ORF prediction intervals
# where field is not present add 0
orf.tab: SNP.filtered.vcf orf_dict.tab nr_hit_dict.tab reference.fasta
	bawk '!/^[$$,\#+]/ {  \
	print $$1; \
	}' $< \
	| bsort --key=1,1 \
	| uniq \
	| translate -a -v -e "0" $^2 1 \
	| translate -a -v -e "0" $^3 1 \
	| translate -a -v <(fastalen $^4) 1 \
	| sed 1'i\#SeqI\tSeqLength\tBlastHit\tORFPrediction' > $@


# output tab delimited file with non-synonymous ad synonymous SNP mutation
# in by dividing them according to which are present within the ORF or 
# the UTR portion.
# (dN_translated+1)/(dS_translated+1) and (dN_untranslated+1)/(dS_untranslated+1) are
# also directly calculated and added to the table. The first line is avoided
# Also produce files variant_region_membership.txt
SNP_KaKs.tab: SNP_var.tab orf.tab reference.fasta
	countKaKsSNPs --variants $< --orfs $^2 --fasta $^3 \
	| bawk '!/^[$$]/ { \
	if (FNR > 1) \
	{ \
	print $$0, ($$2+$$5), ($$3+$$6), ($$2+1)/($$3+1), ($$5+1)/($$6+1), ($$2+$$5+1)/($$3+$$6+1); \
	} \
	else \
	{ print $$0, "dN", "dS", "(dN_translated+1)/(dS_translated+1)", "(dN_untranslated+1)/(dS_untranslated+1)", "(dN+1)/(dS+1)"; } \
	}' > $@

# second file produced by countKaKsSNPs
# contains region in which SNP mutation accours and if non-synonymous or synonymous
variant_region_membership.tab: SNP_KaKs.tab
	touch $@

# add region and non-synonymous/synonymous information to list of SNP
SNP_class_var.tab: SNP_var.tab variant_region_membership.tab
	paste $< $^2 > $@


SNP_class_var.info: SNP_class_var.tab
	bawk 'BEGIN { count_ORF_S=0; count_5UTR_S=0; count_3UTR_S=0; \
	count_ORF_N=0; count_5UTR_N=0; count_3UTR_N=0; \
	print "count_ORF_S", "count_5UTR_S", "count_3UTR_S", \
	"count_ORF_N", "count_5UTR_N", "count_3UTR_N"; } !/^[$$,\#+]/ {  \
	if ($$6 == "ORF" && $$7 == "S") \
	{ count_ORF_S++; } \
	if ($$6 == "5UTR" && $$7 == "S") \
	{  count_5UTR_S++; } \
	if ($$6 == "3UTR" && $$7 == "S") \
	{  count_3UTR_S++; } \
	if ($$6 == "ORF" && $$7 == "N") \
	{ count_ORF_N++; } \
	if ($$6 == "5UTR" && $$7 == "N") \
	{  count_5UTR_N++; } \
	if ($$6 == "3UTR" && $$7 == "N") \
	{  count_3UTR_N++; } \
	} END { print count_ORF_S, count_5UTR_S, count_3UTR_S, \
	count_ORF_N, count_5UTR_N, count_3UTR_N; }' $< > $@


orf.tab.info: orf.tab 
	bawk 'BEGIN { count_blast_hit=0; count_orf=0; count_nothing=0; \
	print "with_blast_hit", "with_orf", "nothing"; } !/^[$$,\#+]/ {  \
	if ($$3 != 0) \
	{ count_blast_hit++; } \
	if ($$4 != 0) \
	{ count_orf++; } \
	if ($$3 == 0 && $$4 == 0) \
	{ count_nothing++; } \
	} END { print count_blast_hit, count_orf, count_nothing; }' $< > $@




SNP_KaKs.info: SNP_KaKs.tab reference.fasta
	_comment () { echo -n ""; }; \
	REF_LEN=`fastalen < $^2 | stat_base --total 2`; \
	_comment "delete the first line which is header"; \
	paste -s -d "\n" \
	<(sed '1d' $< | stat_base --mean --median --stdev --min --max --total 2 \
	| bawk -v ref_len=$$REF_LEN '!/^[$$,\#+]/ {  \
	print "mean_dN_translated/contg\tmedian_dN_translated/contg\tstdev\tmin\tmax\ttotal_dN_translated\ttotal_ref_len\tmean_dN_translated/bp"; \
	if ($$6 == 0) {$$6=1;} \
	printf "%s\t%i\t%.4f\n", $$0, ref_len, ref_len/$$6; \
	}') \
	<(sed '1d' $< | stat_base --mean --median --stdev --min --max --total 3 \
	| bawk -v ref_len=$$REF_LEN '!/^[$$,\#+]/ {  \
	print "mean_dS_translated/contg\tmedian_dS_translated/contg\tstdev\tmin\tmax\ttotal_dS_translated\ttotal_ref_len\tmean_dS_translated/bp"; \
	if ($$6 == 0) {$$6=1;} \
	printf "%s\t%i\t%.4f\n", $$0, ref_len, ref_len/$$6; \
	}') \
	<(sed '1d' $< | stat_base --mean --median --stdev --min --max --total 5 \
	| bawk -v ref_len=$$REF_LEN '!/^[$$,\#+]/ {  \
	print "mean_dN_untranslated/contg\tmedian_dN_untranslated/contg\tstdev\tmin\tmax\ttotal_dN_untranslated\ttotal_ref_len\tmean_dN_untranslated/bp"; \
	if ($$6 == 0) {$$6=1;} \
	printf "%s\t%i\t%.4f\n", $$0, ref_len, ref_len/$$6; \
	}') \
	<(sed '1d' $< | stat_base --mean --median --stdev --min --max --total 6 \
	| bawk -v ref_len=$$REF_LEN '!/^[$$,\#+]/ {  \
	print "mean_dS_untranslated/contg\tmedian_dS_untranslated/contg\tstdev\tmin\tmax\ttotal_dS_untranslated\ttotal_ref_len\tmean_dS_untranslated/bp"; \
	if ($$6 == 0) {$$6=1;} \
	printf "%s\t%i\t%.4f\n", $$0, ref_len, ref_len/$$6; \
	}') \
	<(sed '1d' $< | stat_base --mean --median --stdev --min --max --total 8 \
	| bawk -v ref_len=$$REF_LEN '!/^[$$,\#+]/ {  \
	print "mean_dN/contg\tmedian_dN/contg\tstdev\tmin\tmax\ttotal_dN\ttotal_ref_len\tmean_dN/bp"; \
	if ($$6 == 0) {$$6=1;} \
	printf "%s\t%i\t%.4f\n", $$0, ref_len, ref_len/$$6; \
	}') \
	<(sed '1d' $< | stat_base --mean --median --stdev --min --max --total 9 \
	| bawk -v ref_len=$$REF_LEN '!/^[$$,\#+]/ {  \
	print "mean_dS/contg\tmedian_dS/contg\tstdev\tmin\tmax\ttotal_dS\ttotal_ref_len\tmean_dS/bp"; \
	if ($$6 == 0) {$$6=1;} \
	printf "%s\t%i\t%.4f\n", $$0, ref_len, ref_len/$$6; \
	}') \
	<(sed '1d' $< \
	| bawk 'BEGIN { count_over_1=0; count_under_1=0; print "(Ka+1)/(Ks+1)>1", "(Ks+1)/(Kv+1)<=1";} !/^[$$,\#+]/ { \
	if ($$10 > 1) { count_over_1++; print $$0 > "SNP_KaKs_over1.lst"; } \
	if ($$10 <= 1) { count_under_1++; print $$0 > "SNP_KaKs_below-equal1.lst"; } \
	} END { print count_over_1++, count_under_1++ }') > $@


# this files are generated by the last bawk in the 
# rule of the prerequisite
SNP_KaKs_over1.lst SNP_KaKs_below-equal1.lst: SNP_KaKs.info
	touch $@



Ka-Ks_distrib.pdf: SNP_KaKs.tab
	bawk '!/^[$$,\#+]/ { \
	print log($$10)/log(10); \
	}' $< | Ts-Tv_distrib_plot --type histogram --title "log10 distribution of Ka/Ks for contigs cotaining SNP" --breaks=20 --xlab "log10(Ka/Ks)" --ylab "contigs (#)" --legend-label="SNPs" --color=2 --output=$@ 1



# blast hits of of contigs against Danio CDNAs. From annotation prj
blast_hit.tab: $(DANIO_HIT_TAB)
	ln -sf $< $@

# Total number of genes from match against Danio cDNA
gene.lst: $(GENE_DANIO_HIT_LST)
	ln -sf $< $@
# Total number of cDNA from match against Danio cDNA
CDNA.lst: $(CDNA_DANIO_HIT_LST)
	ln -sf $< $@

# for entichment analysis
background.lst: gene.lst
	cut -d ":" -f 2 $< > $@
background_CDNA.lst: CDNA.lst
	tr -s " " \\t < $< | cut -f 3 > $@



# list of genes from Danio blast hits of contigs with Ka/Ks > 1
%_gene.lst: %.lst blast_hit.tab
	grep --fixed-strings --word-regexp --file <(cut -f1 $< | bsort) <(bsort --key=6,6 $^2) \
	| cut -f 9 \
	| cut -d " " -f 4 \
	| bsort \
	| uniq -c \
	| cut -d ":" -f 2 > $@


# list of cDNAs from Danio blast hits of contigs with KaKs > 1
%_CDNA.lst: %.lst blast_hit.tab
	grep --fixed-strings --word-regexp --file <(cut -f1 $< | bsort) <(bsort --key=6,6 $^2) \
	| cut -f 9 \
	| cut -d " " -f 1 \
	| bsort \
	| uniq -c \
	| tr -s " " \\t \
	| cut -f 3 > $@


# GAF file from Blast2go
b2go.GAF: $(B2GO_TAB)
	ln -sf $< $@


# contig with Ka/Ks > 1 annotatd through Blast2go
%_b2go.lst: %.lst b2go.GAF
	grep --fixed-strings --word-regexp --file <(cut -f1 $< | bsort) <(bawk -v type=$$TIPE '!/^[$$,\#+,\!+]/ {print $$0}' $^2 | bsort --key=2,3) > $@



















.PHONY: test
test:
	@echo $(454_INPUT)


# This should be the default target.
ALL   += reference.fasta \
	 reference.lst \
	 unprocessed.lst \
	 INDEL_summary.lst \
	 INDEL.info \
	 SNP.info \
	 SNP-INDEL_distrib.pdf \
	 INDEL.vcf \
	 INDEL.table \
	 INDEL.removable \
	 INDEL_quality.lst \
	 INDEL.filtered.vcf \
	 SNP_summary.lst \
	 log \
	 var.vcf \
	 $(VAR).flag \
	 SNP.vcf \
	 SNP.table \
	 SNP.removable \
	 SNP_quality.lst \
	 SNP.filtered.vcf \
	 SNP_qual.pdf \
	 Ts-Tv_distrib.pdf \
	 SNP_var.tab \
	 nr_hit_dict.tab \
	 orf_dict.tab \
	 orf.tab \
	 SNP_KaKs.tab \
	 SNP_class_var.tab \
	 SNP_class_var.info \
	 orf.tab.info \
	 SNP_KaKs.info \
	 Ka-Ks_distrib.pdf \
	 SNP_KaKs_over1.lst \
	 SNP_KaKs_below-equal1.lst \
	 SNP_KaKs_over1_gene.lst \
	 SNP_KaKs_over1_CDNA.lst \
	 SNP_KaKs_below-equal1_gene.lst \
	 SNP_KaKs_below-equal1_CDNA.lst \
	 SNP_KaKs_over1_b2go.lst \
	 gene.lst \
	 CDNA.lst \
	 background.lst \
	 background_CDNA.lst \
#	 give errors when executed
#	 SNP_KaKs_below-equal1_b2go.lst \




# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=






# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += reference.fasta \
	 reference.lst \
	 unprocessed.lst \
	 INDEL_summary.lst \
	 INDEL.info \
	 SNP.info \
	 SNP-INDEL_distrib.pdf \
	 INDEL.vcf \
	 INDEL.table \
	 INDEL.removable \
	 INDEL.filtered.vcf \
	 SNP_summary.lst \
	 log \
	 var.vcf \
	 $(VAR).flag \
	 SNP.vcf \
	 SNP.table \
	 SNP.removable \
	 SNP_quality.lst \
	 SNP.filtered.vcf \
	 SNP_qual.pdf \
	 Ts-Tv_distrib.pdf \
	 SNP_var.tab \
	 nr_hit_dict.tab \
	 orf_dict.tab \
	 orf.tab \
	 SNP_KaKs.tab \
	 SNP_class_var.tab \
	 SNP_class_var.info \
	 orf.tab.info \
	 SNP_KaKs.info \
	 Ka-Ks_distrib.pdf \
	 err \
	 INDEL.filtered.vcf.vcfidx \
	 INDEL_quality.lst \
	 INDEL_summary.lst \
	 INDEL.table.bk \
	 INDEL.vcf.idx \
	 nr.tab \
	 orf_predictor.fasta \
	 reference.dict \
	 reference.fasta.fai \
	 SNP.filtered.vcf.vcfidx \
	 SNP_quality \
	 SNP_summary \
	 SNP.table.bk \
	 SNP.vcf.idx \
	 $(VAR) \
	 variant_region_membership.tab \
	 SNP_KaKs_over1.lst \
	 SNP_KaKs_below-equal1.lst \
	 SNP_KaKs_over1_gene.lst \
	 SNP_KaKs_over1_CDNA.lst \
	 SNP_KaKs_below-equal1_gene.lst \
	 SNP_KaKs_below-equal1_CDNA.lst \
	 SNP_KaKs_over1_b2go.lst \
	 SNP_KaKs_below-equal1_b2go.lst \
	 gene.lst \
	 CDNA.lst \
	 background.lst \
	 background_CDNA.lst














# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:




######################################################################
### phase_1.mk ends here
