#!/bin/bash


usage()
{
cat << EOF
usage: $0 options

This script run the test1 or test2 over a machine.

OPTIONS:
   -h      Show this message
   -s      absolute SAM files path
   -r      relative reference path
   -g      relative reference list path
   -l      relative log path
   -v      relative var path
EOF
}

SAM_PATH=
REFERENCE=
REFERENCE_LST=
LOG=
VAR=

while getopts "hs:r:g:l:v:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         s)
             SAM_PATH=$OPTARG
             ;;
         r)
             REFERENCE=$OPTARG
             ;;
         g)
             REFERENCE_LST=$OPTARG
             ;;
         l)
             LOG=$OPTARG
             ;;
         v)
             VAR=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done



if [[ -z $SAM_PATH ]] || [[ -z $REFERENCE ]] || [[ -z $REFERENCE_LST ]] || [[ -z $LOG ]] || [[ -z $VAR ]]
then
     usage
     exit 1
fi


shift $(($OPTIND - 5))
if [ -z $2 ]
then
	echo "ERROR $0: Wrong argument number"
	usage
	exit 1
fi





	_comment () { echo -n ""; }; \
	mkdir -p $LOG; \
	mkdir -p $VAR; \
	_comment "rename with date and time precedent $LOG if exist"; \
	if [ -e $LOG/vcf-validator_freebayes.log ]; then \
	DATE=`date +"%H-%M-%S_%m-%d-%y"`; \
	mv $LOG/vcf-validator_freebayes.log $LOG/$DATE\_vcf-validator_freebayes.log; \
	fi; \
	for FILE in $SAM_PATH/*.sam ; do \
	SAM=$(basename $FILE); \
	SEQ=${SAM%.*}; \
	_comment "test if SAM isn't empty and corresponding .vcf already exist"; \
	if [ -s $FILE ] && [ ! -s $VAR/$SEQ.vcf ]; then \
	_comment "test if SEQ is reference: avoid SAM from singletons"; \
	TEST=`grep --fixed-strings --line-regexp --line-number --no-messages \
	--count $SEQ $REFERENCE_LST`; \
	if [ "$TEST" == "1" ]; then \
	mkdir -p $SEQ && cd $SEQ; \
	_comment "extract SEQ"; \
	get_fasta -i $SEQ < ../$REFERENCE > $SEQ.fasta; \
	_comment "link SAM"; \
	ln -sf $FILE; \
	_comment "left realignment; BAQ adjustment; freebayes SNP detection; \
        validation; trap errors due to freebayes failure: in case of script errors \
        assign error=1"; \
        trap 'error=1;' ERR; \
	samtools faidx $SEQ.fasta && \
	samtools view -but $SEQ.fasta.fai $SAM \
	| bamleftalign -f $SEQ.fasta \
	| samtools calmd -EAru - $SEQ.fasta \
	| freebayes --stdin --fasta-reference $SEQ.fasta --pvar 0.9 --pooled \
	--ploidy 8 --min-alternate-fraction 0.04 --min-alternate-count 2 --min-coverage 5 --no-mnps --no-complex --show-reference-repeats > ../$VAR/$SEQ.vcf; \
	vcf-validator -u ../$VAR/$SEQ.vcf >> ../$LOG/vcf-validator_freebayes.log; \
	_comment "capture exit code failure"; \
	code="$?"; \
	if [[ -n "$error" ]]; then \
	_comment "report failure on $LOG"; \
	echo "[PIPE ERROR] $FILE. Caught an error. Status = $code" >> ../$LOG/vcf-validator_freebayes.log; \
	_comment "create dirs where move files giving errors"; \
	mkdir -p ../err $SAM_PATH/freebayes_err; \
	_comment "move corresponding .vcf (probably incomplete) to error dir"; \
	mv ../$VAR/$SEQ.vcf ../err/; \
	_comment "move SAM giving error to subdir"; \
	mv $FILE $SAM_PATH/freebayes_err/; \
	_comment "remove temp dir"; \
	cd .. && rm -r $SEQ; \
	error=; \
	else  \
	cd .. && rm -r $SEQ; \
	fi; \
	fi; \
	fi \
	done;


