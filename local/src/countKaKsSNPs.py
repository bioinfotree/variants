#!/usr/bin/env python


#countSNPs.py
#John Van Hemert jlv@iastate.edu http://vrac.iastate.edu/~jlv
#Description: counts the synonymous and non-synon. SNPs in a set a sequences with ORFs and variants listed in separate files of the specific format
#used in the supplementary data for the work presented by Tonia Schwartz, et al, BMC Genomics, 2010.
#example usage:
#python countSNPs.py -orfs allContigsMerged_varOnly.Orf.table -variants Add.file8_Variants.txt -fasta 454.mira.merged_contigs_2010-08-04.fna > output.txt
#then run this to add the location of the variants (ORF, UTR, or on the border (5',3')):
#paste -d , variants.txt variant_region_membership.txt > variants_with_regions.txt
#grep ORF variants_with_regions.txt | grep -cv UTR
#grep 5UTR variants_with_regions.txt | grep -cv ORF
#grep 3UTR variants_with_regions.txt | grep -cv ORF
#grep -c 5UTR+ORF variants_with_regions.txt
#grep -c ORF+3UTR variants_with_regions.txt
#
#This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  See http://www.gnu.org/licenses/lgpl.txt



#requires biopython
import sys
from Bio import SeqIO
from Bio import Seq
from Bio import SeqRecord
from Bio.Alphabet import IUPAC
from Bio.Data.CodonTable import TranslationError
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from vfork.util import format_usage



parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter,
			description=format_usage('''
  Orf file: tab delimited file with header; fields are:
  
  #SeqID SeqLength BlastHit ORFPrediction
  contig00001	457	[331-143]	[93-455,+3]

  Variants file: tab separated file with header; fields are:
  
  #SeqID	type	start	end	alleles
  CDNA3-4_11_2010_0_c10029	SNP	102	102	T/C
'''))
parser.add_argument("--orfs", "-r", dest="orfsFilename", required=True, help="orf file path")
parser.add_argument("--variants", "-v", dest="variantsFilename", required=True, help="variant file path")
parser.add_argument("--fasta", "-f", dest="fastaFilename", required=True, help="fasta nucleotides file with")
args = parser.parse_args()


#setup dictionary of orfs and their positions
orfs = dict()

#scan the orfs file (skip the first header line)
orfsFile = open(args.orfsFilename)
first = True
for line in orfsFile:
	if first:
		first = False
		continue
	lineParts = line.strip().split()
	contig = lineParts[0]
	length = int(lineParts[1])
	#skip contigs with no orf predicted
	if len(lineParts[1]) == 0 or lineParts[3].count("[") == 0:
		continue
		sign = "+"
		frame = "1"
		start = 1
		end = length
	else:
		#parse orf start, stop positions and the strand and frame too
		orfParts = lineParts[3].replace("[","").replace("]","").split(",")
		sign = orfParts[1][0]
		frame = orfParts[1][1]
		ends = orfParts[0].split("-")
		start = int(ends[0])
		end = int(ends[1])
	#each orf is a dictionary of start, end, sign(strand), frame, length(contig length), and orf length values
	#they are looked up in the main orf dict
	orf = dict()
	if start<end:
		orf["start"] = start
		orf["end"] = end
	else:
		#orf["end"] = start
		#orf["start"] = end
		rStart = length-start+1
		rEnd = length-end+1
		orf["start"] = rStart
		orf["end"] = rEnd
	orf["sign"] = sign
	orf["frame"] = int(frame)
	orf["length"] = int(length)
	orf["orfLength"] = orf["end"]-orf["start"]+1
	orfs[contig] = orf
orfsFile.close()

#load the fasta sequences into memory
handle = open(args.fastaFilename)
seqs = SeqIO.to_dict(SeqIO.parse(handle, "fasta"))
handle.close()

#this was created to accomodate minus strand orfs that were not already reverse complemented.  current files are already rc'ed so it just returns the sequence
def getSeq(id):
	seq = seqs[id].seq
	if orfs[id]["sign"]=="-":
		seq = seq.reverse_complement()
	#print seq
	return seq

#setup the dictionary of variants
variants = dict()
variantsFile = open(args.variantsFilename)
first = True

regionFile = open("variant_region_membership.tab","w")
regionFile.write("region\tS/N\n")
#setup the dictionarys that will store counts of...
dNt = dict() #...nonsysnonymous snps within the translated region (ORF)
dSt = dict() #...sysnonymous snps within the translated region (ORF)
dNu = dict() #...nonsysnonymous snps within the untranslated region (UTR)
dSu = dict() #...sysnonymous snps within the untranslated region (UTR)
lastContig = ""
#scan the variants file and calculate whether each snp is synonymous
for line in variantsFile:
	#skip the head line
	if first:
		first = False
		continue
	lineParts = line.strip().split()
	#parse contig name/id
	contig = lineParts[0]
	#skip contigs' snps that are not in the orfs file
	if not orfs.has_key(contig):
		regionFile.write("-\t-\n")
		continue
	#initialize counts for new contigs
	if contig != lastContig:
		dNt[contig] = 0
		dSt[contig] = 0
		dNu[contig] = 0
		dSu[contig] = 0
	lastContig = contig
	varType = lineParts[1]
	#parse position of variant
	start = int(lineParts[2])
	stop = int(lineParts[3])
	if orfs[contig]["sign"]=="-":
		rStart = orfs[contig]["length"]-stop+1
		rEnd = orfs[contig]["length"]-start+1
		start = rStart
		stop = rEnd
	#print contig+"\t"+str(start)+"\t"+str(stop)
	#calculate whether the snp is within the predicted ORF or UTR
	startsInORF = start>=orfs[contig]["start"] and start<=orfs[contig]["end"]
	stopsInORF = stop>=orfs[contig]["start"] and stop<=orfs[contig]["end"]
	translated = startsInORF and stopsInORF
	fivePrimeEdge = not(startsInORF) and stopsInORF
	threePrimeEdge = startsInORF and not(stopsInORF)
	fivePrimeUTR = start<=orfs[contig]["start"] and stop<=orfs[contig]["end"]
	threePrimeUTR = start>=orfs[contig]["start"] and stop>=orfs[contig]["end"]
	synon = "-"
	if varType == "SNP": #only work on snps (no indels)
		
		snp = lineParts[4]
		f = snp.split("/")[0]
		t = snp.split("/")[1]
		try: #skip ambiguous base-containing snps
			if orfs[contig]["sign"]=="-":
				f = str(Seq.Seq((f), IUPAC.unambiguous_dna).reverse_complement())
				t = str(Seq.Seq((t), IUPAC.unambiguous_dna).reverse_complement())
			seq = getSeq(contig)
			frame = orfs[contig]["frame"]
			if start>=frame: #skip SNPs that are outside the reading frame scope of the ORF
				#calculate the codon position of the snp
				posFrame = ((start-frame)%3)
				#calculate the start and end of the SNP's codon
				codonStart = start-posFrame-1
				codonEnd = codonStart+3
				#retrieve the codon from the contig sequence based on its start and stop positions
				codon = str(seq[codonStart:codonEnd])
				if not(f==codon[posFrame].upper()): # double check that the base is the same after retrieving the codon 
					if (t==codon[posFrame].upper()): #switch the snp alleles if necessary
						tmp = f
						f = t
						t = tmp
					#else: #(used for debugging during development - never actually happens except for ambiguous dna code)
						#print "ERROR "+snp+" "+codon[posFrame]
						#continue
				#translate the codon (a).  

				aSeq = Seq.Seq((codon), IUPAC.unambiguous_dna)
				#if orfs[contig]["sign"]=="-":
				#	aSeq = aSeq.reverse_complement()
				a = str(aSeq.translate())

				#translate the alternate codon (b)
				alt = list(codon)
				alt[posFrame]=t
				alt = "".join(alt)
				bSeq = Seq.Seq((alt), IUPAC.unambiguous_dna)
				#if orfs[contig]["sign"]=="-":
				#	bSeq = bSeq.reverse_complement()
				b = str(bSeq.translate())
				#print contig+": "+a+" -> "+b+"\t"+str(aSeq)+" -> "+str(bSeq)+"\t"+str(start)+"\t"+str(a==b)
				#increment the appropriate count
				if a==b:
					synon="S"
					if translated:
						dSt[contig]+=1
					else:
						dSu[contig]+=1
				else:
					synon="N"
					if translated:
						dNt[contig]+=1
					else:
						dNu[contig]+=1
		except TranslationError:
			pass#print "ERROR on "+contig
	#write a column stating whether the variant is in the ORF or UTR or runs off the 5' or 3' end
	if translated:
		regionFile.write("ORF\t"+synon+"\n")#"\t"+contig+"\t"+str(start)+"-"+str(stop)+"\t"+str(orfs[contig]["start"])+"-"+str(orfs[contig]["end"])+"\t"+str(getSeq(contig))+"\n")
	elif fivePrimeEdge:
		regionFile.write("5UTR+ORF\t"+synon+"\n")
	elif fivePrimeUTR:
		regionFile.write("5UTR\t"+synon+"\n")
	elif threePrimeEdge:
		regionFile.write("ORF+3UTR\t"+synon+"\n")
	elif threePrimeUTR:
		regionFile.write("3UTR\t"+synon+"\n")
	else:
		regionFile.write("???\t"+synon+"\n")
regionFile.close()
variantsFile.close()

#write results to standard output
print "# contig\tdN_translated\tdS_translated\tORF_length\tdN_untranslated\tdS_untranslated\tUTR_length"
for contig in dSt:
	print contig+"\t"+str(dNt[contig])+"\t"+str(dSt[contig])+"\t"+str(orfs[contig]["orfLength"])+"\t"+str(dNu[contig])+"\t"+str(dSu[contig])+"\t"+str(orfs[contig]["length"]-orfs[contig]["orfLength"])
